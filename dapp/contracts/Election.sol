pragma solidity ^0.4.2;

contract Election {
    string public tipo_de_masa;
    string public queso;
    string public salsa;
    string public tamano;
    string public peperoni;
    string public hawaiana;
    string public jamon;
    string public tocino;
    string public hongos;
    string public salchicha;

    function Election () public {
        tipo_de_masa = "masa";
        queso = "queso";
        salsa = "salsa";
        tamano = "tamano";
        peperoni = "peperoni";
        hawaiana = "hawaiana";
        jamon = "jamon";
        tocino = "tocino";
        hongos = "hongos";
        salchicha = "salchichas";
    }

    function setPizza (string _tipo_de_masa, string _tamano, string _queso, string _salsa, string _peperoni, string _hawaiana, string _jamon, string _tocino, string _hongos, string _salchicha) public {
        tipo_de_masa = _tipo_de_masa;
        queso = _queso;
        salsa = _salsa;
        tamano = _tamano;
        peperoni = _peperoni;
        hawaiana = _hawaiana;
        jamon = _jamon;
        tocino = _tocino;
        hongos = _hongos;
        salchicha = _salchicha;
    }
}
